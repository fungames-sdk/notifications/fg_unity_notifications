# Unity Notification

## Integration Steps

1) To integrate Unity Notifications through FunGames SDK you first need to import the **Mobile Notification package** from the Package Manager.

2) **"Install"** or **"Upload"** FG UnityNotification plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

We also use a prefab called **FGNotificationCSV** to send notifications automatically from a csv file. 